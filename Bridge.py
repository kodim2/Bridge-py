class Figure:
    def square(self):
        raise NotImplementedError()

    def perimeter(self):
        raise NotImplementedError()

    def who_are_you(self):
        raise NotImplementedError()


class Triangle(Figure):
    def __init__(self, z, x, c):
        self.sides = [z, x, c]

    def square(self):
        p = self.perimeter() / 2
        return (p * (p - self.sides[0]) * (p - self.sides[1]) * (p - self.sides[2])) ** 0.5

    def perimeter(self):
        return self.sides[0] + self.sides[1] + self.sides[2]

    def who_are_you(self):
        return "Треугольник"


class Rectangle(Figure):
    def __init__(self, z, x):
        self.sides = [z, x]

    def square(self):
        return self.sides[0] * self.sides[1]

    def perimeter(self):
        return (self.sides[0] + self.sides[1]) * 2

    def who_are_you(self):
        return "Прямоугольник"


class Square(Rectangle):
    def __init__(self, z):
        super().__init__(z, z)

    def who_are_you(self):
        return "Квадрат"


class Color:
    def get_name(self):
        raise NotImplementedError()

    def get_code(self):
        raise NotImplementedError()


class Red(Color):
    def get_name(self):
        return "Красный"

    def get_code(self):
        return 255, 0, 0


class Blue(Color):
    def get_name(self):
        return "Синий"

    def get_code(self):
        return 0, 0, 255


class RedTriangle(Triangle, Red):
    def who_are_you(self):
        return self.get_name() + " " + super().who_are_you()


class RedRectangle(Rectangle, Red):
    def who_are_you(self):
        return self.get_name() + " " + super().who_are_you()


class RedSquare(Square, Red):
    def who_are_you(self):
        return self.get_name() + " " + super().who_are_you()


class BlueTriangle(Triangle, Blue):
    def who_are_you(self):
        return self.get_name() + " " + super().who_are_you()


class BlueRectangle(Rectangle, Blue):
    def who_are_you(self):
        return self.get_name() + " " + super().who_are_you()


class BlueSquare(Square, Blue):
    def who_are_you(self):
        return self.get_name() + " " + super().who_are_you()


def main():
    # Создаем экземпляры различных классов
    triangle = RedTriangle(3, 4, 5)
    rectangle = RedRectangle(2, 6)
    square = RedSquare(4)

    blue_triangle = BlueTriangle(5, 12, 13)
    blue_rectangle = BlueRectangle(3, 8)
    blue_square = BlueSquare(6)

    # Выводим информацию о каждой фигуре
    print(triangle.who_are_you())
    print(f"Площадь: {triangle.square()}")
    print(f"Периметр: {triangle.perimeter()}")

    print(rectangle.who_are_you())
    print(f"Площадь: {rectangle.square()}")
    print(f"Периметр: {rectangle.perimeter()}")

    print(square.who_are_you())
    print(f"Площадь: {square.square()}")
    print(f"Периметр: {square.perimeter()}")

    print(blue_triangle.who_are_you())
    print(f"Площадь: {blue_triangle.square()}")
    print(f"Периметр: {blue_triangle.perimeter()}")

    print(blue_rectangle.who_are_you())
    print(f"Площадь: {blue_rectangle.square()}")
    print(f"Периметр: {blue_rectangle.perimeter()}")

    print(blue_square.who_are_you())
    print(f"Площадь: {blue_square.square()}")
    print(f"Периметр: {blue_square.perimeter()}")


if __name__ == "__main__":
    main()
